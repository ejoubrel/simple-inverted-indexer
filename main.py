"""Main runner for the app."""
from src.indexer import Indexer
from src.services import FieldEnum
import spacy
from src.enums import TokenStrategies


def main():
    """Main function running the app"""
    indexer = Indexer(
        webpages_filepath="input/crawled_urls.json",
        field=FieldEnum.TITLE,
        language=spacy.load('fr_core_news_md'),
        token_strategy=TokenStrategies.STEM
    )
    indexer.create_metadata()
    indexer.create_index()
    indexer.save_index()


if __name__ == "__main__":
    main()