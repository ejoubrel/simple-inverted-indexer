# Simple Indexer

This project implements a simple app to create inverted indexes.
Given a list of crawled webpages, split into fields, it can create several types of inverted indexes :
- Simple non-positional indexes
- Count indexes : count occurences of entry in each document it appears in
- Positional indexes : list all positions of token for each document it appears in

Said tokens can either be just tokens, or be lemmatized by `spacy`, stemmed by `nltk`, or both.

All those settings can be chosen in the main runner through Enums.

To reduce computing time, results of nlp processes (using `spacy`) are stored in json files. This allows to fetch those data when working on the same collection. On the downside, saving or retrieving from those json files might use some computing power.

Whenever you change the input file, make sure to force the nlp process so that the data is downloaded once again.

Each created index, along with general metadata about the collection, is stored under a detailed name in json files in `/input`.


## Authors

- Elwenn Joubrel : [@ejoubrel](https://www.gitlab.com/ejoubrel)


## Installation

You will need python and pip, and after cloning this project:

```bash
  cd simple-inverted-indexer
  pip install -r requirements.txt
  python app.main
```


## To develop
- Find a better way to store nlp process so it's lighter to compute.
- Add a lot more metadata and statistics, possibly about the détails of created index.