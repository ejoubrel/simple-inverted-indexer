"""This module lists all Enum classes used through the app.

Classes
-------
FieldEnum(Enum)
    Lists possible fields in webpages
IndexStrategies(Enum)
    Lists strategies for indexing
TokenStrategies(Enum)
    Lists strategies for computing tokens
"""
from enum import Enum

from src.token_strategies import *
from src.indexing_strategies import *


class FieldEnum(Enum):
    """Lists all possible fields in webpages"""
    TITLE = 'title'
    CONTENT = 'content'
    HEADER = 'h1'


class IndexingStrategies(Enum):
    """Lists all strategies for indexing."""
    STANDARD = IndexingStandardStrategy()
    POSITIONAL = IndexingPositionStrategy()
    COUNT = IndexingCountStrategy()


class TokenStrategies(Enum):
    """Lists all strategies for computing tokens."""
    STANDARD = TokenStandardStrategy()
    LEMMA = TokenLemmatizeStrategy()
    STEM = TokenStemStrategy()
    LEMMA_STEM = TokenLemmatizeAndStemStrategy()
