"""This module lists functions used to compute statistics.

Those functions can compute metadata for the whole pages collections, or
related to a specific index.

Function
--------
count_tokens_in_field(webpages, field, language)
    Count the number of tokens in field in all web pages.
"""
from typing import List, Dict

from spacy import language
import spacy

from src.enums import FieldEnum
from src.services import get_docs


def count_tokens_in_field(webpages: List[Dict[str, str]],
                          field: FieldEnum,
                          language: spacy.Language) -> int:
    """Count the number of tokens in field in all web pages.

    Parameters
    ----------
    webpages : List[Dict[str, str]]
        The collection of web pages
    field : FieldEnum
        The field to study
    language : spacy.Language
        The spacy language to use

    Returns
    -------
    int
        The number of tokens
    """
    docs = get_docs(webpages, field, language)
    return sum([len(doc) for doc in docs])
