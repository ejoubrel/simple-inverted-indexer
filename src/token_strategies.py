"""This module implements Strategy pattern for computing tokens.

Classes
-------
TokenStrategy(ABC)
    Abstract strategy
TokenStandardStrategy(TokenStrategy):
    Standard strategy for processing token, i.e not treatment.
TokenLemmatizeStrategy(TokenStrategy):
    Concrete Strategy for lemmatizing tokens.
TokenStemStrategy(TokenStrategy):
    Concrete strategy for stemming tokens.
TokenLemmatizeAndStemStrategy(TokenStrategy):
    Concrete strategy for lemmatizing and stemming tokens.
"""
from abc import ABC, abstractmethod

from nltk.stem import SnowballStemmer
from spacy.tokens import Token


class TokenStrategy(ABC):
    """Abstract Strategy for computing tokens."""
    
    @abstractmethod
    def process_token(self, token: Token) -> str:
        """Abstract method to enforce method definition in subclasses."""
        pass
    
    @abstractmethod
    def build_filename_part(self) -> str:
        """Abstract method to enforce method definition in subclasses."""
        pass


class TokenStandardStrategy(TokenStrategy):
    """Standard strategy for processing token, i.e not treatment."""
    
    def process_token(self, token: Token) -> str:
        """Processes the token to get the index entry

        Parameters
        ----------
        token : Token
            The spacy token to process

        Returns
        -------
        str
            The final entry to use in the index
        """
        return token.lower()
    
    def build_filename_part(self) -> str:
        """Creates the part of the output file name

        Returns
        -------
        str
            Part of the output file name
        """
        return ""


class TokenLemmatizeStrategy(TokenStrategy):
    """Concrete Strategy for lemmatizing tokens."""
    
    def process_token(self, token: Token) -> str:
        """Processes the token to get the index entry

        Parameters
        ----------
        token : Token
            The spacy token to process

        Returns
        -------
        str
            The final entry to use in the index
        """
        return token.lemma_.lower()
    
    def build_filename_part(self) -> str:
        """Creates the part of the output file name

        Returns
        -------
        str
            Part of the output file name
        """
        return "lemma."


class TokenStemStrategy(TokenStrategy):
    """Concrete strategy for stemming tokens."""
    
    def process_token(self, token: Token) -> str:
        """Processes the token to get the index entry

        Parameters
        ----------
        token : Token
            The spacy token to process

        Returns
        -------
        str
            The final entry to use in the index
        """
        stemmer = SnowballStemmer('french')
        return stemmer.stem(token.text)
    
    def build_filename_part(self) -> str:
        """Creates the part of the output file name

        Returns
        -------
        str
            Part of the output file name
        """
        return "stem."


class TokenLemmatizeAndStemStrategy(TokenStrategy):
    """Concrete strategy for lemmatizing and stemming tokens."""
    
    def process_token(self, token: Token) -> str:
        """Processes the token to get the index entry

        Parameters
        ----------
        token : Token
            The spacy token to process

        Returns
        -------
        str
            The final entry to use in the index
        """
        stemmer = SnowballStemmer('french')
        return stemmer.stem(token.lemm_)
    
    def build_filename_part(self) -> str:
        """Creates the part of the output file name

        Returns
        -------
        str
            Part of the output file name
        """
        return "stem.lemma."
