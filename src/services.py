"""This module implements functions used to handle files in the app.

Functions
---------
load_webpages(webpages_filepath)
    Load webpages from json file
get_docs(webpages, field, language, force_creation)
    Get documents of chosen field or create them.
"""
from typing import List, Dict
import json
import os

from spacy.tokens import Doc
import spacy

from src.enums import FieldEnum


def _create_documents(field: FieldEnum,
                     webpages: List[Dict[str, str]],
                     language: spacy.Language) -> List[Doc]:
    """Create spacy documents of said field from web pages.

    Parameters
    ----------
    field : FieldEnum
        The chosen field
    webpages : List[Dict[str, str]]
        The webpages under study
    language : spacy.Language
        The spacy language to use

    Returns
    -------
    List[Doc]
        The created documents
    """
    docs = language.pipe([page[field.value] for page in webpages],
                           disable=['tagger', 'parser', 'ner', 'textcat'])
    return list(docs)


def _save_docs(docs: List[Doc],
              filename: str) -> None:
    """Save spacy documents in json file.

    Parameters
    ----------
    docs : List[Doc]
        The list of documents to save
    filename : str
        The name of the file to save them in
    """
    with open(filename, 'w', encoding='utf-8') as file:
        json.dump([doc.to_json() for doc in docs], file)


def _load_docs(filename: str,
              language: spacy.Language) -> List[Doc]:
    """Load spacy documents from json file.

    Parameters
    ----------
    filename : str
        Name of the json file that stores documents.
    language : spacy.Language
        The spacy language in use

    Returns
    -------
    List[Doc]
        The list of spacy documents.
    """
    with open(filename, 'r', encoding='utf-8') as file:
        docs = json.load(file)
    return [Doc(language.vocab).from_json(doc) for doc in docs]


def _filename_from_field(field: FieldEnum) -> str:
    """Recreate file name from field.
    
    Create a name for the json file to store documents from chosen field. 

    Parameters
    ----------
    field : FieldEnum
        The chosen field studied.

    Returns
    -------
    str
        The json file name.
    """
    return f"data/{field.value}_list.json"


def load_webpages(webpages_filepath: str) -> List[Dict[str, str]]:
    """Load web pages from input json file

    Parameters
    ----------
    webpages_filepath : str
        The path to the json file

    Returns
    -------
    List[Dict[str, str]]
        The list of webpages fetched in file
    """
    with open(webpages_filepath, 'r', encoding='utf-8') as file:
        list_pages = json.load(file)
    return list_pages


def get_docs(webpages: List[Dict[str, str]],
             field: FieldEnum,
             language: spacy.Language,
             force_creation: bool = False) -> List[Doc]:
    """Get documents of chosen field or create them.
    
    If documents (pages processed by spacy) have been created, fetches them
    from json file. Otherwise, creates and saves them in said file.

    Parameters
    ----------
    webpages : List[Dict[str, str]]
        The collection of pages
    field : FieldEnum
        The chosen field
    language : spacy.Language
        The spacy language in use
    force_creation : bool, optional
        True if we want to force creating the documents, by default False

    Returns
    -------
    List[Doc]
        _description_
    """
    filename = _filename_from_field(field)
    if force_creation or not os.path.isfile(filename):
        docs = _create_documents(field, webpages, language)
        _save_docs(docs, filename)
    return _load_docs(filename, language)
