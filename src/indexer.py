"""This module implements the main class of the app, the indexer.

Classes
-------
Indexer
    Class to create and save indexes and metadata.
"""
import json
import os

from nltk.stem import SnowballStemmer
from spacy.tokens import Token
import spacy

from src.compute_statistics import count_tokens_in_field
from src.enums import FieldEnum, IndexingStrategies, TokenStrategies
from src.services import load_webpages, get_docs


class Indexer:
    """Main class of the app, to create and save indexes and metadata
    
    Attributes
    ----------
    field : FieldEnum
        Field to consider in index.
    language : spacy.Language
        The spacy language to use.
    webpages: dict
        All web pages in collection.
    indexing_strategy : IndexingStrategies, optional
        Indexing strategy to use, by default standard non-positional
    token_strategy : TokenStrategies, optional
        Token computation strategy to use, by default no treatment
    force_creation : bool, optional
        True if wanting to force re-creating documents, by default False
    docs: List[Doc]
        All field documents to index.
    index: dict
        The created index, empty if not created.
        
    Methods
    -------
    create_index(self) -> None:
        Create index using selected strategy.
    save_index(self) -> None:
        Save created index in json file.
    create_metadata(self) -> None:
        Create json file containing metadata of pages collection.
    """
    
    def __init__(
        self,
        webpages_filepath: str,
        field: FieldEnum,
        language: spacy.Language,
        indexing_strategy: IndexingStrategies = IndexingStrategies.STANDARD,
        token_strategy: TokenStrategies = TokenStrategies.STANDARD,
        force_new_nlp_process = False
    ):
        self.field = field
        self.language = language
        
        self.webpages = load_webpages(webpages_filepath)
        self.force_creation = force_new_nlp_process

        self.docs = get_docs(self.webpages,
                             field,
                             language,
                             force_new_nlp_process)
        
        self.indexing_strategy = indexing_strategy.value
        self.indexing_strategy.strategy = token_strategy.value
        
        self.index = {}

    def _get_output_filename(self) -> str:
        """Create filename for output file for the index.

        Returns
        -------
        str
            Path to output file
        """
        token_proc = self.indexing_strategy.strategy.build_filename_part()
        index_proc = self.indexing_strategy.build_filename_part()
        return "output/" + token_proc + self.field.value + "." + index_proc

    def create_index(self) -> None:
        """Create index using selected strategy."""
        self.index = self.indexing_strategy.create_index(self.docs)
    
    def save_index(self) -> None:
        """Save created index in json file."""
        filename = self._get_output_filename()
        with open(filename, 'w', encoding='utf-8') as file:
            json.dump(self.index, file)

    def _compute_metadata(self) -> dict:
        """Compute metadata and statistics about the pages collection.

        Returns
        -------
        dict
            Computed metadata
        """
        metadata = {}
        nb_docs = len(self.webpages)
        metadata['nb_documents'] = nb_docs
        metadata['nb_tokens_per_field'] = {'document': 0}
        metadata['avg_tokens_per_field'] = {}
        for field in FieldEnum:
            nb_tokens = count_tokens_in_field(self.webpages,
                                              field,
                                              self.language)
            metadata['nb_tokens_per_field']['document'] += nb_tokens
            metadata['nb_tokens_per_field'][field.value] = nb_tokens
            metadata['avg_tokens_per_field'][field.value] = nb_tokens / nb_docs
        avg_tok_doc = metadata['nb_tokens_per_field']['document'] / nb_docs
        metadata['avg_tokens_per_field']['document'] = avg_tok_doc
        return metadata
    
    def create_metadata(self) -> None:
        """Create json file containing metadata of pages collection."""
        metadata = self._compute_metadata()
        with open('output/metadata.json', 'w', encoding='utf-8') as file:
            json.dump(metadata, file)
