"""This module implements the Strategy pattern for creating indexes.

Classes
-------
IndexingStrategy(ABC):
    Abstract strategy for indexing.
IndexingStandardStrategy(IndexingStrategy):
    Concrete strategy for non-position indexes.
IndexingPositionStrategy(IndexingStrategy):
    Concrete strategy for creating position indexes.
IndexingCountStrategy(IndexingStrategy):
    Concrete strategy for creating count indexes.
"""
from abc import ABC, abstractmethod
from typing import List

from spacy.tokens import Token, Doc

from src.token_strategies import TokenStrategy


class IndexingStrategy(ABC):
    """Abstract strategy for indexing."""
    
    @property
    def strategy(self) -> TokenStrategy:
        """Getter for strategy"""
        return self._strategy
    
    @strategy.setter
    def strategy(self, token_strategy: TokenStrategy) -> None:
        """Setter for strategy"""
        self._strategy = token_strategy

    
    def process_token(self, token: Token) -> str:
        """Processes the token to get the index entry

        Parameters
        ----------
        token : Token
            The spacy token to process

        Returns
        -------
        str
            The final entry to use in the index
        """
        self.strategy.process_token(token)
    
    @abstractmethod
    def create_index(self, docs: List[Doc]) -> dict:
        """Abstract method to enforce method definition in subclasses."""
        pass

    @abstractmethod
    def build_filename_part(self) -> str:
        """Abstract method to enforce method definition in subclasses."""
        pass


class IndexingStandardStrategy(IndexingStrategy):
    """Concrete strategy for non-position indexes."""
    
    def create_index(self, docs: List[Doc]) -> dict:
        """Creates a non-position index from documents.

        Parameters
        ----------
        docs : List[Doc]
            The list of documents to compute.

        Returns
        -------
        dict
            The created index
        """
        index = {}
        for i, doc in enumerate(docs):
            for token in doc:
                if token.is_alpha and not token.is_stop:
                    key = self.process_token(token)
                    if key not in index.keys():
                        index[key] = [i]
                    else:
                        if i not in index[key]:
                            index[key].append(i)
        return index
    
    def build_filename_part(self) -> str:
        """Creates the part of the output file name

        Returns
        -------
        str
            Part of the output file name
        """
        return ("non_pos_index.json")


class IndexingPositionStrategy(IndexingStrategy):
    """Concrete strategy for creating position indexes."""
    
    def create_index(self, docs: List[Doc]) -> dict:
        """Creates a position index from documents.

        Parameters
        ----------
        docs : List[Doc]
            The list of documents to compute.

        Returns
        -------
        dict
            The created index
        """
        index = {}
        for i, title in enumerate(docs):
            for token_index in range(len(title)):
                token = title[token_index]
                if token.is_alpha and not token.is_stop:
                    key = self.process_token(token)
                    if key not in index.keys():
                        index[key] = {i: [token_index]}
                    else:
                        if i not in index[key]:
                            index[key][i] = [token_index]
                        else:
                            index[key][i].append(token_index)
        return index

    def build_filename_part(self) -> str:
        """Creates the part of the output file name

        Returns
        -------
        str
            Part of the output file name
        """
        return "pos_index.json"


class IndexingCountStrategy(IndexingStrategy):
    """Concrete strategy for creating count indexes."""
    
    def create_index(self, docs: List[Doc]) -> dict:
        """Creates a count index from documents.

        Parameters
        ----------
        docs : List[Doc]
            The list of documents to compute.

        Returns
        -------
        dict
            The created index
        """
        index = {}
        for i, title in enumerate(docs):
            for token in title:
                if not token.is_stop and token.is_alpha:
                    key = self.process_token(token)
                    if key not in index.keys():
                        index[key] = {i: 1}
                    else:
                        if i not in index[key]:
                            index[key][i] = 1
                        else:
                            index[key][i] += 1
        return index
    
    def build_filename_part(self) -> str:
        """Creates the part of the output file name

        Returns
        -------
        str
            Part of the output file name
        """
        return "count_non_pos_index.json"
